package com.example.kathrynfameronag.midtermact2fameronag;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView Image1 = (ImageView) findViewById(R.id.JCW);
        Image1.animate().alpha(1f).setDuration(3000);

        Thread timer = new Thread() {
            public void run(){
                try {
                   sleep(5000);
                } catch(InterruptedException e){
                    e.printStackTrace();
                }
                finally{
                    Intent i = new Intent(MainActivity.this,Main2Activity.class);
                    startActivity(i);
                    finish();
                }
            }
        }; timer.start();
    }
}
